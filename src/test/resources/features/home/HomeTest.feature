Feature: Home Test

Scenario Outline: : Home Test Evolution Game
  Given on page view
  When change language to russia
  And click menu header title
  And click search menu
  And fill search field <text>
  And choose search subheading - sell
  And click submit search button
  And click sort by price
  And choose sales type sale
  And click advance search
  And fill minimum price <minprice>
  And fill maximum price <maxprice>
  And click submit search button
  And click 3 ads
  And click add to memo
  And click ok on alert
  And click bookmark menu
  Then verify ads exist in bookmark page

  Examples:
  | text | minprice | maxprice |
  | Computer | 160 | 300 |