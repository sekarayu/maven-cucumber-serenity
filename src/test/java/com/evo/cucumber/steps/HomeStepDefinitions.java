package com.evo.cucumber.steps;

import com.evo.cucumber.pages.HomePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by sekarayu on 4/4/17.
 */
public class HomeStepDefinitions {

    public HomePage homePage;

    @Given("on page view")
    public void onPageView(){
        homePage.openPage();
    }

    @When("change language to russia")
    public void changelangtoru(){
        homePage.clickChangeLanguage();
    }

    @When("click menu header title")
    public void clickmenuheadertitle(){
        homePage.clickHeaderTitle();
    }

    @When("click search menu")
    public void clicksearchmenu(){ homePage.clickSearch(); }

    @When("fill search field (.*)")
    public void fillSearchField(String text){ homePage.fillSearchField(text); }

    @When("choose search subheading - sell")
    public void chooseSubheadSell(){ homePage.selectSubheadingSell(); }

    @When("click submit search button")
    public void clickSubmitSearch(){ homePage.clickSearchSubmit(); }

    @When("click sort by price")
    public void sortByPrice(){ homePage.clickSortByPrice(); }

    @When("choose sales type sale")
    public void chooseSalesTypeSale(){ homePage.chooseSalesType(); }

    @When("click advance search")
    public void clickAdvSearch(){ homePage.clickAdvSearch(); }

    @When("fill minimum price (.*)")
    public void fillMinPrice(String minprice){ homePage.fillMinPrice(minprice); }

    @When("fill maximum price (.*)")
    public void fillMaxPrice(String maxprice){ homePage.fillMaxPrice(maxprice);}

    @When("click 3 ads")
    public void click3ads(){ homePage.click3ads(); }

    @When("click add to memo")
    public void clickaddtomemo(){ homePage.clickAddtomemo();}

    @When("click ok on alert")
    public void clickOkAlert(){ homePage.clickOkOnAlert(); }

    @When("click bookmark menu")
    public void clickBookmarkMenu(){ homePage.clickBookmarkmenu(); }

    @Then("verify ads exist in bookmark page")
    public void verifyadsexist(){ homePage.verifyAdExistInBookmarkPage(); }
}
