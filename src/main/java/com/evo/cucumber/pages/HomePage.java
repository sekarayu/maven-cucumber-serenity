package com.evo.cucumber.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.junit.Assert;
import org.openqa.selenium.support.ui.Select;

import java.util.logging.Logger;

/**
 * Created by sekarayu on 4/4/17.
 */
@DefaultUrl("https://www.ss.lv")
public class HomePage extends PageObject{
    @FindBy(xpath = "//*[@id=\"main_table\"]/span[3]/a")
    public WebElementFacade changeLanguage;

    @FindBy(xpath = "//*[@id=\"td_6\"]/div/div/div[1]/div/h2/a")
    public WebElementFacade headerTitle;

    @FindBy(xpath = "//*[@id=\"page_main\"]/tbody/tr/td/div[1]/div[1]/h2/a")
    public WebElementFacade sectionTitle;

    @FindBy(xpath = "//*[@id=\"main_table\"]/span[2]/b[3]/a")
    public WebElementFacade search;

    @FindBy(id = "ptxt")
    public WebElementFacade searchField;

    @FindBy(id = "sbtn")
    public WebElementFacade searchSubmitBtn;

    @FindBy(xpath = "//*[@id=\"page_main\"]/tbody/tr/td/div[1]/div[1]/h2/b")
    public WebElementFacade searchResultTitle;

    @FindBy(id = "cmp_1")
    public WebElementFacade searchSuggestion;

    @FindBy(xpath = "//*[@id=\"head_line\"]/td[2]/noindex/a")
    public WebElementFacade sortPrice;

    @FindBy(xpath = "//*[@id=\"page_main\"]/tbody/tr/td/table[1]/tbody/tr/td[4]/a")
    public WebElementFacade advSearch;

    @FindBy(name = "topt[8][min]")
    public WebElementFacade minPrice;

    @FindBy(name = "topt[8][max]")
    public WebElementFacade maxPrice;

    @FindBy(id = "c41484051")
    public WebElementFacade checkbox1;

    @FindBy(id = "dm_41484051")
    public WebElementFacade titleAds1;

    @FindBy(id = "c41483967")
    public WebElementFacade checkbox2;

    @FindBy(id = "dm_41483967")
    public WebElementFacade titleAds2;

    @FindBy(id = "c35663137")
    public WebElementFacade checkbox3;

    @FindBy(id = "dm_35663137")
    public WebElementFacade titleAds3;

    @FindBy(id = "a_fav_sel")
    public WebElementFacade addToMemo;

    @FindBy(id = "alert_ok")
    public WebElementFacade alertOk;

    @FindBy(xpath = "//*[@id=\"main_table\"]/span[2]/span/b/a")
    public WebElementFacade bookmarkMenu;

    Logger logger = Logger.getLogger("info");
    String ads1title, ads2title, ads3title = "";
    private String url = "https://www.ss.lv";

    public void openPage(){
        this.getDriver().get(url);
        this.getDriver().manage().window().maximize();
    }

    public void clickChangeLanguage(){
        changeLanguage.click();
        Assert.assertTrue(this.getDriver().getCurrentUrl().contains("ru"));
    }

    public void clickHeaderTitle(){
        String title = headerTitle.getText();
        headerTitle.click();
        Assert.assertTrue(title.equals(sectionTitle.getText()));
    }

    public void clickSearch(){
        search.click();
        Assert.assertTrue(this.getDriver().getCurrentUrl().contains("search"));
    }

    public void fillSearchField(String text){
        searchField.type(text);
        waitABit(3000);
        logger.info("visible search suggestion : "+searchSuggestion.isCurrentlyVisible());
        if(searchSuggestion.isCurrentlyVisible()){
            searchSuggestion.click();
        }
    }

    public void selectSubheadingSell(){
        this.getDriver().findElement(By.name("sid")).click();
        Select subhead = new Select(this.getDriver().findElement(By.name("sid")));
        subhead.selectByVisibleText("Продают");
    }

    public void clickSearchSubmit(){
        searchSubmitBtn.click();
        Assert.assertTrue(searchResultTitle.isDisplayed());
    }

    public void clickSortByPrice(){
        sortPrice.click();
    }

    public void chooseSalesType(){
        this.getDriver().findElement(By.xpath("//*[@id=\"page_main\"]/tbody/tr/td/div[2]/span[3]/select")).click();
        waitABit(3000);
        Select salestype = new Select(this.getDriver().findElement(By.xpath("//*[@id=\"page_main\"]/tbody/tr/td/div[2]/span[3]/select")));
        salestype.selectByVisibleText("Продажа");
    }

    public void clickAdvSearch(){
        advSearch.click();
        Assert.assertTrue(this.getDriver().getCurrentUrl().contains("search"));
    }

    public void fillMinPrice(String min){
        minPrice.type(min);
    }

    public void fillMaxPrice(String max){
        maxPrice.type(max);
    }

    public void click3ads(){
        checkbox1.click();
        checkbox2.click();
        checkbox3.click();
        ads1title = titleAds1.getText();
        ads2title = titleAds2.getText();
        ads3title = titleAds3.getText();
        waitABit(4000);
    }

    public void clickAddtomemo(){
        addToMemo.click();
    }

    public void clickOkOnAlert(){
        if(alertOk.isDisplayed()){
            alertOk.click();
        }
    }

    public void clickBookmarkmenu(){
        bookmarkMenu.click();
    }

    public void verifyAdExistInBookmarkPage(){
        Assert.assertTrue(titleAds1.isVisible() && ads1title.contains(titleAds1.getText()));
        Assert.assertTrue(titleAds2.isVisible() && ads2title.contains(titleAds2.getText()));
        Assert.assertTrue(titleAds3.isVisible() && ads3title.contains(titleAds3.getText()));
    }
}
